首先回顾了dockers里的一些重要内容

# Docker

在进行华为云实践中学习了docker的一些基本命令，但是不是很熟悉，因此回顾了docker中最重要的内容dockerfile

## dockerfile 编写

```bash
$ mkdir mynginx
$ cd mynginx
$ touch Dockerfile
```

内容为

```bash
FROM nginx
RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
```

由于dockerfile中每一个指令都会建立一层，每一个 `RUN` 的行为，会新建立一层，在其上执行这些命令，执行结束后，`commit` 这一层的修改，构成新的镜像。镜像是多层存储，每一层的东西并不会在下一层被删除，会一直跟随着镜像。因此镜像构建时，一定要确保每一层只添加真正需要添加的东西，任何无关的东西都应该清理掉。

## 构建镜像

在 `Dockerfile` 文件所在目录执行：

```bash
[root@supercomputer]# docker build -t nginx:v3 .
Sending build context to Docker daemon 2.048 kB
Step 1 : FROM nginx
 ---> e43d811ce2f4
Step 2 : RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
 ---> Running in 9cdc27646c7b
 ---> 44aa4490ce2c
Removing intermediate container 9cdc27646c7b
Successfully built 44aa4490ce2c
```

## Docker指令

`FROM` 就是指定 **基础镜像**，因此一个 `Dockerfile` 中 `FROM` 是必备的指令，并且必须是第一条指令。

`RUN <命令>`，就像直接在命令行中输入的命令一样

`COPY` 指令将从构建上下文目录中 `<源路径>` 的文件/目录复制到新的一层的镜像内的 `<目标路径>` 位置。比如：

```bash
COPY package.json /usr/src/app/
COPY hom* /mydir/
COPY hom?.txt /mydir/
```

`ADD` 指令和 `COPY` 的格式和性质基本一致。如果 `<源路径>` 为一个 `tar` 压缩文件的话，压缩格式为 `gzip`, `bzip2` 以及 `xz` 的情况下，`ADD` 指令将会自动解压缩这个压缩文件到 `<目标路径>` 去。

在使用该指令的时候还可以加上 `--chown=<user>:<group>` 选项来改变文件的所属用户及所属组。

```bash
ADD --chown=55:mygroup files* /mydir/
ADD --chown=bin files* /mydir/
ADD --chown=1 files* /mydir/
ADD --chown=10:11 files* /mydir/
```

## CMD 容器启动命令

`CMD` 指令的格式和 `RUN` 相似，也是两种格式：

- `shell` 格式：`CMD <命令>`
- `exec` 格式：`CMD ["可执行文件", "参数1", "参数2"...]`

- 参数列表格式：`CMD ["参数1", "参数2"...]`。在指定了 `ENTRYPOINT` 指令后，用 `CMD` 指定具体的参数。

  

在运行时可以指定新的命令来替代镜像设置中的这个默认命令，比如，`ubuntu` 镜像默认的 `CMD` 是 `/bin/bash`，如果我们直接 `docker run -it ubuntu` 的话，会直接进入 `bash`。我们也可以在运行时指定运行别的命令，如 `docker run -it ubuntu cat /etc/os-release`。这就是用 `cat /etc/os-release` 命令替换了默认的 `/bin/bash` 命令了，输出了系统版本信息。



在指令格式上，一般推荐使用 `exec` 格式，这类格式在解析时会被解析为 JSON 数组，因此一定要使用双引号 `"`，而不要使用单引号。



如果使用 `shell` 格式的话，实际的命令会被包装为 `sh -c` 的参数的形式进行执行。比如：

```
CMD echo $HOME
```

在实际执行中，会将其变更为：

```bash
CMD [ "sh", "-c", "echo $HOME" ]
```

这就是为什么我们可以使用环境变量的原因，因为这些环境变量会被 shell 进行解析处理



`ENTRYPOINT` 的格式和 `RUN` 指令格式一样，分为 `exec` 格式和 `shell` 格式。

`ENTRYPOINT` 的目的和 `CMD` 一样，都是在指定容器启动程序及参数。`ENTRYPOINT` 在运行时也可以替代，不过比 `CMD` 要略显繁琐，需要通过 `docker run` 的参数 `--entrypoint` 来指定。



`EXPOSE` 指令是声明运行时容器提供服务端口，这只是一个声明，在运行时并不会因为这个声明应用就会开启这个端口的服务。在 Dockerfile 中写入这样的声明有两个好处，一个是帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射；另一个用处则是在运行时使用随机端口映射时，也就是 `docker run -P` 时，会自动随机映射 `EXPOSE` 的端口。



`WORKDIR` 指令可以来指定工作目录（或者称为当前目录），以后各层的当前目录就被改为指定的目录，如该目录不存在，`WORKDIR` 会帮你建立目录.

`USER` 指令和 `WORKDIR` 相似，都是改变环境状态并影响以后的层。`WORKDIR` 是改变工作目录，`USER` 则是改变之后层的执行 `RUN`, `CMD` 以及 `ENTRYPOINT` 这类命令的身份。

# k8s

## Kubernetes优势

- 自动装箱,水平扩展,自我修复
- 服务发现和负载均衡

- 自动发布(默认滚动发布模式)和回滚
- 集中化配置管理和密钥管理

- 存储编排
- 任务批处理运行

## Pod

- Pod是K8S里能够被运行的最小的逻辑单元(原子单元)
- 1个Pod里面可以运行多个容器,它们共享UTS+NET +IPC名称空间

- 可以把Pod理解成豌豆荚,而同- -Pod内的每个容器是一 颗颗豌豆
- 一个Pod里运行多个容器,又叫:边车(SideCar)模式

## Pod控制器

- Pod控制器是Pod启动的一-种模板,用来保证在K8S里启动的Pod
- 应始终按照人们的预期运行(副本数、生命周期、健康状态检查... )

- K8S内提供了众多的Pod控制器,常用的有以下几种:

Deployment

DaemonSet （每个节点起一份）

ReplicaSet  （Deployment管 ReplicaSet，ReplicaSet管pod）

StatefulSet  （管理有状态应用的）

Job

Cronjob

## Name

- 由于K8S内部,使用“资源”来定义每一种逻辑概念(功能)故每种"资源”, 都应该有自己的"名称”
- "资源”有api版本( apiVersion )类别( kind )、元数据( metadata)、定义清单( spec)、状态( status )等配置信息

- "名称”通常定义在"资源”的"元数据”信息里

## Namespace

- 随着项目增多、人员增加、集群规模的扩大,需要- -种能够隔离K8S内各种"资源”的方法，这就是名称空间
- 名称空间可以理解为K8S内部的虚拟集群组

- 不同名称空间内的"资源”名称可以相同,相同名称空间内的同种“资源”，”名称” 不能相同
- 合理的使用K8S的名称空间,使得集群管理员能够更好的对交付到K8S里的服务进行分类管理和浏览

- K8S里默认存在的名称空间有: default、 kube-system、 kube-public
- 查询K8S里特定“资源”要带上相应的名称空间

## Label

- 标签是k8s特色的管理方式,便于分类管理资源对象。
- 一个标签可以对应多个资源，-个资源也可以有多个标签,它们是多对多的关系。

- 一个资源拥有多个标签,可以实现不同维度的管理。
- 标签的组成: key=value(值不能多余64个字节字母数字开头 中间只能是 - _ .）

- 与标签类似的,还有一种“注解” ( annotations )

## Label选择器

- 给资源打上标签后,可以使用标签选择器过滤指定的标签
- 标签选择器目前有两个:基于等值关系(等于、不等于)和基于集合关系(属于、不属于、存在)

- 许多资源支持内嵌标签选择器字段

matchl _abels

matchExpressions

## Service

- 在K8S的世界里,虽然每个Pod都会被分配一个单独的IP地址,但这个IP地址会随着Pod的销毁而消失
- Service (服务)就是用来解决这个问题的核心概念

- 一个Service可以看作- -组提供相同服务的Pod的对外访问接口
- Service作用于哪些Pod是通过标签选择器来定义的

## Ingress

- Ingress是K8S集群里工作在OSI网络参考模型下,第7层的应用,对外暴露的接口
- Service只能进行L4流量调度,表现形式是ip+port

- Ingress则可以调度不同业务域、 不同URL访问路径的业务流量

## 组件

![image-20210820200105978](C:\Users\wys\AppData\Roaming\Typora\typora-user-images\image-20210820200105978.png)

## 核心组件

配置存储中心→etcd服务

高可用奇数个

## 主控( master )节点

### kube-apiserver

- 提供了集群管理的RESTAPI接口(包括鉴权、数据校验及集群状态变更)
- 负责其他模块之间的数据交互,承担通信枢纽功能

- 是资源配额控制的入口
- 提供完备的集群安全机制

### kube-controller-manager

- 由一系列控制器组成,通过apiserver监控整个集群的状态,并确保集群处于预期的工作状态

Node Controller

Deployment Controller

Service Controller

Volume Controller

Endpoint Controller

Garbage Controller

Namespace Controller

Job Controller

Resource quta Controller

### kube-scheduler

- 主要功能是接收调度pod到适合的运算节点上
- 预选策略( predict )

- 优选策略( priorities )

  ## 运算(node)节点

  ### kube- kubelet

  - 简单地说, kubelet的主要功能就是定时从某个地方获取节点上pod的期望状态(运行什么容器、运行的副本数量、网络或者存储如何配置等等) ,并调用对应的容器平台接口达到这个状态
  - 定时汇报当前节点的状态给apiserver,以供调度的时候使用

  - 镜像和容器的清理工作，保证节点上镜像不会占满磁盘空间，退出的容器不会占用太多资源

  ### Kube-proxy

  - 是K8S在每个节点 上运行网络代理, service资源的载体
  - **建立了pod网络和集群网络的关系**( clusterip >podip )

  - 常用三种流量调度模式

  Userspace (废弃)

  Iptables (濒临废弃)（绝大部分公司在用）

  Ipvs(推荐)

  - 负责建立和删除包括更新调度规则、通知apiserver自己的更新,或者从apiserver哪里获取其他kube- proxy的调度规则变化来更新自己的

  ## CLI客户端

  ### kubectl

  Kubernetes 命令行工具，`kubectl`，使得你可以对 Kubernetes 集群运行命令。 你可以使用 `kubectl` 来部署应用、监测和管理集群资源以及查看日志。

  # 核心附件

  ### CNI网络插件→flannel/calico

  k8s设计了网络模型，但却将他的实现交给了网络插件，CNI网络插件最主要的功能就是实现POD资源能够跨主机进行通信

  种类众多，以`**flannel**`为例

  三种常用工作模式

  优化SNAT规则

  ### 服务发现用插件→coredns

  服务（应用）之间相互定位的过程

  集群网络> Cluster IP

  Service资源→Service Name .

  `**Coredns**`软件>实现了Service Name和Cluster IP的自动关联

  ### 服务暴露用插件→traefik

  Ingress资源→专用于暴露7层应用到K8S集群外的一-种核心资源(http/https)

  Ingress控制器>一个简化版的nginx (调度流量) + go脚本(动态识别yaml)

  `**Traefik**`软件→实现了ingress控制器的一个软件

  ### GUI管理插件→Dashboard